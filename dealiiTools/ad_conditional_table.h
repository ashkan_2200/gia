/*
 * A conditional table based on the deal.II table.
 * It is usefull for instance to shorten the code
 * to only add table row for one of the processors
 * in a multiprocessor run.
 */
#ifndef _DISTRIBUTED_TABLE_H
#define _DISTRIBUTED_TABLE_H

#include <deal.II/base/table_handler.h>
#include <string>
#include <iostream>

namespace dealiiTools {
class ConditionalTable
{
public:
    ConditionalTable(bool condition = true):
    _condition{condition}
    {};

    void set_condition(bool condition){
        _condition = condition;
    }

    template<class T>
    void add_value(const std::string &key, const T &value){
        if(_condition)
            _table.add_value(key, value);
    }

    void write_text(std::ostream &stream) const{
        if(_condition)
            _table.write_text(stream);
    }

    void clear(){
        if(_condition)
            _table.clear();
    }
    
    void clear_current_row(){
        if(_condition)
            _table.clear_current_row();
    }

private:
    bool _condition;
    dealii::TableHandler _table;
};
}

#endif
