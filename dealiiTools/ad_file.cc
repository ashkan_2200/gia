#include "ad_file.h"

#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>

using namespace dealiiTools;

void File::write_matrix(dealii::LinearAlgebraTrilinos::MPI::SparseMatrix &M, const char *filename){
    FILE *fp;
    fp = fopen(filename,"wb");

    int n   = M.n();
    int m   = M.m();
    int nnz = M.n_nonzero_elements();

    Epetra_CrsMatrix mtr = M.trilinos_matrix();

    fwrite(&n, sizeof(int), 1, fp);
    fwrite(&m, sizeof(int), 1, fp);
    fwrite(&nnz, sizeof(int), 1, fp);

    fwrite(mtr.ExpertExtractIndexOffset().Values(), sizeof(int), m+1, fp);
    fwrite(mtr.ExpertExtractIndices().Values(), sizeof(int), nnz, fp);
    fwrite(mtr.ExpertExtractValues(), sizeof(double), nnz, fp);

    fclose(fp);
}

void File::write_matrix(const dealii::FullMatrix<double> &M, const char *filename)
{
    //Printing in matlab form
    std::ofstream matFile (filename);
    M.print(matFile,10);
    matFile.close();
}

void File::write_vector(const dealii::LinearAlgebraTrilinos::MPI::Vector &V, const char *filename)
{
    FILE *fp;
    fp = fopen(filename,"wb");

    int n   = V.size();

    Epetra_MultiVector vtr = V.trilinos_vector();

    fwrite(&n, sizeof(int), 1, fp);

    fwrite(vtr.Values(), sizeof(double), n, fp);

    fclose(fp);
}

void dealiiTools::File::ex2matlab(dealii::LinearAlgebraTrilinos::MPI::SparseMatrix &M, const char *matlab_var_name)
{
    std::stringstream ss;
    ss << matlab_var_name << ".dat";
    write_matrix(M, ss.str().c_str());
    ss.str("");


    ss << "get_" << matlab_var_name << ".m";
    std::ofstream myfile;
    myfile.open(ss.str().c_str());
    if(!myfile.is_open()){
        throw std::runtime_error("Unable to open file...");
    }

    ss.str("");

    ss << "fileID = fopen('"
       << matlab_var_name
       << ".dat');\n"
          "n = fread(fileID,1,'int');\n"
          "m = fread(fileID,1,'int');\n"
          "nnz=fread(fileID,1,'int');\n"
          "ia = fread(fileID,m+1,'int');\n"
          "ja = fread(fileID,nnz,'int');\n"
          "va = fread(fileID,nnz,'double');\n"
          "ia = ia+1;\n"
          "ja = ja+1;\n"
          "II = ja*0;\n"
          "for i=1:m\n"
          "   for j=ia(i):ia(i+1)-1\n"
          "       II(j) = i;\n"
          "   end\n"
          "end\n"
          "\n"
       << matlab_var_name << " = "
          "sparse(II,ja,va,m,n);\n"
          "fclose(fileID);\n"
          "clear i ia va II j ja m n nnz fileID;\n"
       << std::endl;
    myfile << ss.str();

    myfile.close();


}

void dealiiTools::File::ex2matlab(const dealii::LinearAlgebraTrilinos::MPI::Vector &V, const char *matlab_var_name)
{
    std::stringstream ss;
    ss << matlab_var_name << ".dat";
    write_vector(V, ss.str().c_str());
    ss.str("");
    ss << "get_" << matlab_var_name << ".m";
    std::ofstream myfile;
    myfile.open(ss.str().c_str());
    if(!myfile.is_open()){
        throw std::runtime_error("Unable to open file...");
    }

    ss.str("");

    ss << "fileID = fopen('"
       << matlab_var_name
       << ".dat');\n"
          "n = fread(fileID,1,'int');\n"
       << matlab_var_name << " = "
          "fread(fileID,n,'double');\n"
          "fclose(fileID);\n"
          << std::endl;
    myfile << ss.str();

    myfile.close();
}
