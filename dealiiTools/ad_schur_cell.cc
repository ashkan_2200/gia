
#include "ad_schur_cell.h"

using namespace dealiiTools;

SchurCell::SchurCell(const dealii::BlockInfo &bf, const unsigned int dof_per_cell) :
    _bf(bf),
    _bi(_bf.local()),
    _dof_per_cell(dof_per_cell)
{
    dim_p_l = _bi.block_size(_bi.size()-1);
    dim_u_l = _dof_per_cell - dim_p_l;

    cell_ordered.reinit(_dof_per_cell, _dof_per_cell);
    l_A.reinit(dim_u_l,dim_u_l);
    l_Bt.reinit(dim_u_l,dim_p_l);
    l_B.reinit(dim_p_l,dim_u_l);
    l_C.reinit(dim_p_l,dim_p_l);
    order = std::vector<unsigned int>(_dof_per_cell);

    for(auto i = 0u; i < _dof_per_cell; i++)
        order[_bf.renumber(i)] = i;
}

void SchurCell::construct_schur(dealii::FullMatrix<double> &cell_matrix,
                           dealii::FullMatrix<double> &cell_precond,
                           const double h)
{
    cell_precond = cell_matrix;
    cell_ordered.fill_permutation(cell_matrix, order, order);

    // Fill small local matrices corresponding to A11, Bt, B and C
    for (unsigned int i = 0u; i < dim_u_l; i++)
        for (unsigned int j = 0u; j < dim_u_l; j++)
            l_A(i,j) = cell_ordered(i,j);

    for (unsigned int i = dim_u_l; i < _dof_per_cell; i++)
        for (unsigned int j = dim_u_l; j < _dof_per_cell; j++)
            l_C(i-dim_u_l,j-dim_u_l) = cell_ordered(i,j);

    for (unsigned int i = dim_u_l; i < _dof_per_cell; i++)
        for (unsigned int j = 0u; j < dim_u_l; j++)
            l_B(i-dim_u_l,j) = cell_ordered(i,j);

    for (unsigned int i = 0u; i < dim_u_l; i++)
        for (unsigned int j = dim_u_l; j < _dof_per_cell; j++)
            l_Bt(i,j-dim_u_l) = cell_ordered(i,j);

    l_A.diagadd(h*h);
    // Compute inverse
    l_A.gauss_jordan();

    // Compute local Schur
    l_C.triple_product(l_A, l_B, l_Bt, false, false, -1.0);

    // Place local Schur back in the unordered matrix
    for (unsigned int i = dim_u_l; i < _dof_per_cell; i++)
        for (unsigned int j = dim_u_l; j < _dof_per_cell; j++)
            cell_precond(order[i], order[j]) = l_C(i-dim_u_l,j-dim_u_l);
}
