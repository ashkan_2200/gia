/*
 * Author :  Ali Dorostkar
 * Date   :  2016-09-12
 * Feature: .This is a simple binder to lua interface which makes calling methods from lua that return values easier.
 */
#ifndef __AD_LUA_FUNCTION_H_
#define __AD_LUA_FUNCTION_H_

#include <stdexcept>
#include <iostream>
#include <map>
#include <deal.II/base/point.h>
#include <deal.II/lac/vector.h>

extern "C"{
#include <lua.h>                                /* Always include this when calling Lua */
#include <lauxlib.h>                            /* Always include this when calling Lua */
#include <lualib.h>                             /* Prototype for luaL_openlibs(), */
}

namespace dealiiTools {
typedef std::map<std::string, double> Globals;

template<int inDIM, int outDIM = inDIM, class number=double>
class LuaFunction
{
public:
    // Initiate the lua state and openlibs
    LuaFunction(void) {
        L = luaL_newstate();
        luaL_openlibs(L);
        _filename = "";
    }
    // close lua state at the end
    ~LuaFunction(void){
        lua_close(L);
    }
    // If there is an open file already, close it.
    // Open another file and call the initiating function.
    void init_file(const std::string &filename){
        if(_filename != ""){
            lua_close(L);
            _filename = "";
        }
        _filename = filename;
        if (luaL_loadfile(L, _filename.c_str()))
            bail("luaL_loadfile() failed");
        if (lua_pcall(L, 0, 0, 0))                  /* PRIMING RUN. FORGET THIS AND YOU'RE TOAST */
            bail("lua_pcall() failed");
        lua_pushnil(L);
    }
    // change the name of the method.
    void init_method(const std::string &methodname){
        _methodname = methodname;
    }

    void vector_value(const dealii::Point<inDIM, number> &p, dealii::Vector<number> &values) const{
        Assert (values.size() >= outDIM, dealii::ExcIndexRange (values.size(), 0, outDIM));

        lua_getglobal(L, _methodname.c_str());
        for(int i = 0; i < inDIM; i++)
            lua_pushnumber(L, p[i]); /* Push all the neccessary inputs to lua stack. These will be used as the function input */

        /*
         * Call lua function loaded at the top of this method, with inDIM input arguments,
         * Expect outDIM output arguments.
         */
        if (lua_pcall(L, inDIM, outDIM, 0))
            bail("lua_pcall() failed");

        for(int i = 0; i < outDIM; i++)
            values[i] = lua_tonumber(L, -outDIM + i);
        lua_pop(L, outDIM);
    }
    void set_global(const std::string &name, const double val){
        lua_pushnumber(L, val);
        lua_setglobal(L, name.c_str());
    }
    void set_global(const Globals &m){
        for(auto i = m.begin(); i != m.end();i++)
            set_global(i->first, i->second);
    }

private:
    // throw an error with appropriate message
    void bail(const std::string &msg) const{
        lua_close(L);
        std::string ss = std::string("\n LuaFunction FATAL ERROR: ") + msg;
        throw std::runtime_error(ss);
    }
    std::string _filename;
    std::string _methodname;
    lua_State *L;
};
}

#endif
