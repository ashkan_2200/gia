#ifndef FILE_H
#define FILE_H

#include <deal.II/lac/generic_linear_algebra.h>

namespace dealiiTools {
class File{
public:
    // Create bindary matrix files and write appropriate
    // Matlab m files to read it.
    static void ex2matlab(dealii::LinearAlgebraTrilinos::MPI::SparseMatrix &M, const char *matlab_var_name);
    static void ex2matlab(const dealii::LinearAlgebraTrilinos::MPI::Vector &V, const char *matlab_var_name);

    // Creat binary matrix files
    static void write_matrix(dealii::LinearAlgebraTrilinos::MPI::SparseMatrix &M, const char *filename);
    static void write_vector(const dealii::LinearAlgebraTrilinos::MPI::Vector &V, const char *filename);

    // Create normal matrix file for a full matrix
    static void write_matrix(const dealii::FullMatrix<double> &M, const char *filename);
};
}

#endif
