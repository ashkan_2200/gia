#ifndef __AD_SCHUR_H_
#define __AD_SCHUR_H_

#include <deal.II/dofs/block_info.h>
#include <deal.II/lac/full_matrix.h>

namespace dealiiTools {
class SchurCell{
public:
    SchurCell(const dealii::BlockInfo &bf, const unsigned int dof_per_cell);
    void construct_schur(dealii::FullMatrix<double> & cell_matrix,
                    dealii::FullMatrix<double> & cell_precond,
                    const double h);
private:
    dealii::BlockInfo _bf;
    dealii::BlockIndices _bi;
    unsigned int _dof_per_cell;
    unsigned int dim_u_l, dim_p_l;

    dealii::FullMatrix<double>	cell_ordered,
                                l_A, l_Bt, l_B, l_C;

    std::vector<unsigned int> order;
};
}

#endif
