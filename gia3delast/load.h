/* TODO

*/

#ifndef LOAD_H
#define LOAD_H

#include <deal.II/base/function.h>

#include "config.h"
#include "parameters.h"
#include "ad_lua_function.h"

namespace GeoPhysics
{
    // Neumann Boundary conditions
    class Load: public dealii::Function<DIM>
    {
    public:
        Load (parameters *_par);

        virtual double value (const dealii::Point<DIM>   &p, const unsigned int  component = 0) const;

        virtual void vector_value (const dealii::Point<DIM> &p, dealii::Vector<double>   &values) const;
    private:
        // pointer to parameter object
        parameters *par;
        dealiiTools::LuaFunction<DIM, DIM+1> lf;
    };
}

#endif
