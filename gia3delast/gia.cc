#include "gia.h"

#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/std_cxx11/bind.h>
#include <deal.II/base/utilities.h>
#include <deal.II/base/utilities.h>
#include <deal.II/distributed/grid_refinement.h>
#include <deal.II/distributed/grid_refinement.h>
#include <deal.II/distributed/solution_transfer.h>
#include <deal.II/dofs/block_info.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/manifold_lib.h>
#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/solver_gmres.h>
#include <deal.II/lac/sparsity_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/data_out_faces.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/vector_tools.h>

#include "ad_conditional_table.h"
#include "ad_file.h"
#include "ad_schur_cell.h"
#include "coefficients.h"
#include "load.h"
#include "preconditioner.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>
#include <vector>

using namespace GeoPhysics;
using namespace dealii;
using std::endl;
using std::stringstream;
using std::string;
using std::ostringstream;
using std::ofstream;

gia::gia(parameters *_par) :
    par(_par),
    mpi_communicator(MPI_COMM_WORLD),
    pcout(std::cout,
          (Utilities::MPI::this_mpi_process(mpi_communicator) == 0)),
    timer(mpi_communicator,
          pcout,
          TimerOutput::summary,
          TimerOutput::wall_times),
    triangulation (mpi_communicator,
                   typename Triangulation<DIM>::MeshSmoothing
                   (Triangulation<DIM>::smoothing_on_refinement |
                    Triangulation<DIM>::smoothing_on_coarsening)),
    dof_handler (triangulation),
    fe (FE_Q<DIM>(par->mesh.degree+1), DIM, // Displacement
        FE_Q<DIM>(par->mesh.degree), 1), // pressure
    dofs_per_block(std::vector<types::global_dof_index>(NBM))
{}

gia::~gia()
{
    dof_handler.clear();
}

void gia::initiate(){
    stringstream ss;

#ifdef LOGRUN
    pcout << "Logging: " << ALERT("Enabled") << endl;
#else
    pcout << "Logging: " << TELL("Disabled") << endl;
#endif

#ifdef USE_TRILINOS
    pcout << "Solver: " << TELL("Trilinos") << endl;
#elif USE_PETSC
    pcout << "Solver: " << TELL("PETSc") << endl;
#else
    pcout << "Solver: " << ALERT("Unknown") << endl;
#endif

    par->prettyPrint(ss);
    pcout << ss.str();
}

void gia::makeGrid(){
    pcout << PRINT_HEADER("Generate grid") << endl;
    TimerOutput::Scope t(timer, "Grid generator");
    // Number of initial subdivisions for each axis
    static SphericalManifold<DIM> surface_description;
    GridGenerator::hyper_shell(triangulation,
                               Point<DIM>(), // center is at zero
                               par->earth.innerRadi,
                               par->earth.outerRadi,
                               par->mesh.n_initial_cells,
                               true);

    triangulation.set_all_manifold_ids(0);
    triangulation.set_manifold(0, surface_description);

    // Refine the mesh with the number of refinement in the parameters.
    triangulation.refine_global(2);
    triangulation.refine_global(par->mesh.n_refs);
}

void gia::setupDofs(){
    pcout << PRINT_HEADER("Setup DOFs") << endl;
    // Set scope of the timer
    TimerOutput::Scope t(timer, "Setup DOFs");

    dof_handler.distribute_dofs (fe);

    // Renumber component wise
    std::vector<unsigned int> gcomponents (DIM+1, 0);
    gcomponents[DIM] = 1;

    // DOF renumbering
    DoFRenumbering::component_wise (dof_handler, gcomponents);

    // Get number of dofs in components
    DoFTools::count_dofs_per_block (dof_handler, dofs_per_block, gcomponents);

    // Output number of dofs
    const unsigned int  n_u = dofs_per_block[0],
            n_p = dofs_per_block[1];

    locally_owned_dofs = dof_handler.locally_owned_dofs();
    owned_partitioning.clear();
    relevant_partitioning.clear();
    owned_partitioning.push_back(locally_owned_dofs.get_view(0, n_u));
    owned_partitioning.push_back(locally_owned_dofs.get_view(n_u, n_u+n_p));

    DoFTools::extract_locally_relevant_dofs (dof_handler,
                                             locally_relevant_dofs);

    relevant_partitioning.push_back(locally_relevant_dofs.get_view(0,n_u));
    relevant_partitioning.push_back(locally_relevant_dofs.get_view(n_u,n_u+n_p));



    //Interpolate boudaries using constraint matrix
    {
        constraints.clear();
        constraints.reinit(locally_relevant_dofs);

        DoFTools::make_hanging_node_constraints (dof_handler,
                                                 constraints);

        ComponentMask ns_mask = fe.component_mask(FEValuesExtractors::Vector(0)); //NO_SLIP

        // set inner boundary to non moving
        VectorTools::interpolate_boundary_values (dof_handler,
                                                  0,
                                                  ZeroFunction<DIM>(DIM+1),
                                                  constraints,
                                                  ns_mask);

        constraints.close();
    }

    system_matrix.clear();
    system_precon.clear();

#if defined(USE_PETSC)
    BlockDynamicSparsityPattern dsp(dofs_per_block, dofs_per_block);

    DoFTools::make_sparsity_pattern(dof_handler,
                                    dsp,
                                    constraints, true);


    SparsityTools::distribute_sparsity_pattern (dsp,
                                                dof_handler.locally_owned_dofs_per_processor(),
                                                mpi_communicator,
                                                locally_relevant_dofs);

    system_matrix.reinit(owned_partitioning,
                         dsp,
                         mpi_communicator);

    system_precon.reinit(owned_partitioning[1],
                         dsp.block(1,1),
                         mpi_communicator);
#elif defined(USE_TRILINOS)
    TrilinosWrappers::BlockSparsityPattern sp(owned_partitioning, owned_partitioning,
                                              relevant_partitioning,
                                              MPI_COMM_WORLD);


    Table<2,DoFTools::Coupling> coupling (DIM+1, DIM+1);
    for (unsigned int c=0; c<DIM+1; ++c)
        for (unsigned int d=0; d<DIM+1; ++d)
            coupling[c][d] = DoFTools::always;
    DoFTools::make_sparsity_pattern (dof_handler,
                                     coupling, sp,
                                     constraints, true,
                                     Utilities::MPI::
                                     this_mpi_process(MPI_COMM_WORLD));

    sp.compress();

    system_matrix.reinit(sp);
    system_precon.reinit(sp.block(1,1));
#endif

    locally_relevant_solution.reinit (owned_partitioning,
                                      relevant_partitioning,
                                      mpi_communicator);
    system_rhs.reinit (owned_partitioning,
                       mpi_communicator);
}

void gia::assemble(){
    pcout << PRINT_HEADER("Assembling") << endl;
    // Set scope of the timer
    TimerOutput::Scope t(timer, "Assembling");

    // Define quadrature rule and degree
    QGauss<DIM>   quadrature_formula(par->mesh.degree+2);
    QGauss<DIM-1> face_quadrature_formula(par->mesh.degree+2);
    FEValues<DIM> fe_values (fe, quadrature_formula,
                             update_values    |
                             update_quadrature_points  |
                             update_JxW_values |
                             update_gradients);

    FEFaceValues<DIM> fe_face_values (fe, face_quadrature_formula,
                                      update_values    |
                                      update_normal_vectors |
                                      update_quadrature_points  |
                                      update_JxW_values);
    const unsigned int   dofs_per_cell   = fe.dofs_per_cell;

    const unsigned int   n_q_points      = quadrature_formula.size();
    const unsigned int   n_face_q_points = face_quadrature_formula.size();
    // Block information for reordering and preconditioner computing.
    dof_handler.initialize_local_block_info();

    // Initialize the class that knows how to construct Schur complement
    // on each cell
    dealiiTools::SchurCell schurConstructor(dof_handler.block_info(), dofs_per_cell);

    FullMatrix<double>          cell_matrix  (dofs_per_cell, dofs_per_cell),
            cell_precond (dofs_per_cell, dofs_per_cell);

    Vector<double>              cell_rhs (dofs_per_cell);

    std::vector<unsigned int>   local_dof_indices (dofs_per_cell);

    Load                        ice(par);
    std::vector<Vector<double> >iceVal (n_face_q_points, Vector<double>(DIM+1));
    Coefficients                coeff(par);
    std::vector<double>         nu_values (n_q_points);
    double mu, beta;

    const FEValuesExtractors::Vector displacements (0);
    const FEValuesExtractors::Scalar pressure (DIM);
    Tensor<1,DIM> e;
    //    e = 1;
    for (int i = 0; i < DIM; i++)
        e[i] = 1.0;

    std::vector<SymmetricTensor<2,DIM> > symgrad_phi_u	(dofs_per_cell);
    std::vector<Tensor<2,DIM> >          grad_phi		(dofs_per_cell);
    std::vector<Tensor<1,DIM> >			 phi_u			(dofs_per_cell);
    std::vector<double>                  div_phi_u		(dofs_per_cell);
    std::vector<double>                  phi_p			(dofs_per_cell);

    system_matrix = 0;
    system_precon = 0;
    system_rhs = 0;

    typename DoFHandler<DIM>::active_cell_iterator
            cell = dof_handler.begin_active(),
            endc = dof_handler.end();
    for (; cell!=endc; ++cell) {
        // If not in this processor, move on
        if(!cell->is_locally_owned()) continue;

        fe_values.reinit (cell);
        cell_matrix		= 0;
        cell_rhs		= 0;
        cell_precond	= 0;

        // Get coefficient valye for nu on the points of this cell
        coeff.value_list     (fe_values.get_quadrature_points(), nu_values);

        // Compute on quadrature points
        for (auto q=0u; q<n_q_points; ++q) {
            for (auto k=0u; k < dofs_per_cell; ++k) {
                symgrad_phi_u[k] = fe_values[displacements].symmetric_gradient (k, q);
                grad_phi[k]      = fe_values[displacements].gradient (k, q);
                phi_u[k]		 = fe_values[displacements].value (k, q);
                div_phi_u[k]     = fe_values[displacements].divergence (k, q);
                phi_p[k]         = fe_values[pressure].value (k, q);
            }

            mu = par->earth.young/(2*(1+nu_values[q]));
            beta = par->earth.young*(1-2*nu_values[q])/(4*nu_values[q]*(1+nu_values[q]));
            for (auto i=0u; i<dofs_per_cell; ++i){
                for (auto j=0u; j < dofs_per_cell; ++j){
                    cell_matrix(i,j) += (
                                symgrad_phi_u[i] * symgrad_phi_u[j] * 2 * mu         // A
                                - grad_phi[j]  * e * phi_u[i] * par->scales.RHORGL_S * par->switchs.enable_advection	// A-adv
                                + div_phi_u[j] * e * phi_u[i] * par->scales.RHORGL_S * par->switchs.enable_divergance	// A-div
                                + div_phi_u[i] * phi_p[j] * mu                       // Bt
                                + phi_p[i] * div_phi_u[j] * mu                       // B
                                - phi_p[i] * phi_p[j] * beta                         // C
                                )* fe_values.JxW(q);

                }// end j

                cell_rhs(i) +=  phi_u[i] * e * par->weight * fe_values.JxW(q); // load vector, body force
            }// end i
        } // end q
        // Neumann Boundary conditions (Ice-Load and free surface)
        for (auto face_num=0u; face_num<GeometryInfo<DIM>::faces_per_cell; ++face_num){
            if (cell->face(face_num)->at_boundary()
                    && (cell->face(face_num)->boundary_id() == 1 ) ){
                // Update face values
                fe_face_values.reinit (cell, face_num);
                // Update boundary values
                ice.vector_value_list(fe_face_values.get_quadrature_points(),
                                      iceVal);
                for (auto q=0u; q < n_face_q_points; ++q){
                    for (auto i=0u; i<dofs_per_cell; ++i){
                        const unsigned int
                                component_i = fe.system_to_component_index(i).first;

                        cell_rhs(i) +=  fe_face_values.shape_value(i, q) *
                                iceVal[q](component_i) *
                                fe_face_values.JxW(q);
                    }
                }
            }// end if at boundary
        }// end face

        schurConstructor.construct_schur(cell_matrix, cell_precond, cell->diameter());

        // end assembly A preconditioner

        // local-to-global
        cell->get_dof_indices (local_dof_indices);
        constraints.distribute_local_to_global(cell_matrix, cell_rhs,
                                               local_dof_indices,
                                               system_matrix,
                                               system_rhs);


        for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
            for (unsigned int j=0; j<dofs_per_cell; ++j)
            {
                int idx = local_dof_indices[i] - dofs_per_block[0];
                int idy = local_dof_indices[j] - dofs_per_block[0];
                if (idx >= 0 && idy >= 0)
                    system_precon.add (idx, idy,
                                       cell_precond(i,j));
            }
        }
        // end local-to-global
    }
    system_matrix.compress(VectorOperation::add);
    system_precon.compress(VectorOperation::add);
    system_rhs.compress(VectorOperation::add);
}

void gia::setup_preconditioner(){
    // Set scope of the timer
    pcout << PRINT_HEADER("Setup preconditioner") << endl;
    TimerOutput::Scope t(timer, "Setup preconditioner");

    A_preconditioner
            = std_cxx1x::shared_ptr<LA::MPI::PreconditionAMG>(new LA::MPI::PreconditionAMG());

    S_preconditioner
            = std_cxx1x::shared_ptr<LA::MPI::PreconditionAMG>(new LA::MPI::PreconditionAMG());


    LA::MPI::PreconditionAMG::AdditionalData amg_A;
    LA::MPI::PreconditionAMG::AdditionalData amg_S;

#if defined(USE_TRILINOS)
    // Extract constant modes, Important to know how many components are used
    std::vector<std::vector<bool> > constant_modes;
    FEValuesExtractors::Vector disp_components(0); //Extract dissplacement components

    DoFTools::extract_constant_modes (dof_handler,
                                      fe.component_mask(disp_components),
                                      constant_modes);

    amg_A.constant_modes = constant_modes;
    amg_A.elliptic = true;
    amg_A.higher_order_elements = false;
    amg_A.smoother_sweeps = 2;
    amg_A.aggregation_threshold = par->amg.threshold;
    amg_A.output_details = par->amg.make_output;


    //

    amg_S.elliptic = true;
    amg_S.higher_order_elements = false;
    amg_S.smoother_sweeps = 2;
    amg_S.aggregation_threshold = par->amg.threshold;
    amg_S.output_details = par->amg.make_output;
#endif
    A_preconditioner->initialize( system_matrix.block(0,0),amg_A);
    // elem-by-elem Schur
    S_preconditioner->initialize( system_precon, amg_S);
}

void gia::solve(){
    pcout << PRINT_HEADER("Solve") << endl;
    TimerOutput::Scope t(timer, "Solve");

    Preconditioner prc( system_matrix,
                        system_precon,
                        *A_preconditioner,
                        *S_preconditioner,
                        owned_partitioning,
                        mpi_communicator,
                        par);

    prc.set_doSolve(false);

    LA::MPI::BlockVector distributed_solution(owned_partitioning, mpi_communicator);
    // Needed for solution transfer.
    distributed_solution = locally_relevant_solution;
    try{
        SolverControl solver_control (par->sys.n_amg_mult,
                                      1e-12*system_rhs.l2_norm());


#ifdef LOGRUN
        solver_control.enable_history_data();
        solver_control.log_history(true);
        solver_control.log_result(true);
#endif

        SolverFGMRES<LA::MPI::BlockVector>
                solver (solver_control,
                        SolverFGMRES<LA::MPI::BlockVector>::AdditionalData(par->sys.restart)); // With restart of 100

#ifdef LOGRUN
        deallog.push("Outer");
#endif

        constraints.set_zero(distributed_solution);
        solver.solve(system_matrix,
                     distributed_solution,
                     system_rhs,
                     prc);

#ifdef LOGRUN
        deallog.pop();
#endif

        par->sys.itrs.push_back(solver_control.last_step());
    }
    catch(SolverControl::NoConvergence){

        SolverControl solver_control (system_matrix.m(),
                                      par->sys.tol*system_rhs.l2_norm());

        prc.set_doSolve(true);

#ifdef LOGRUN
        solver_control.enable_history_data();
        solver_control.log_history(true);
        solver_control.log_result(true);
#endif

        SolverFGMRES<LA::MPI::BlockVector>
                solver (solver_control,
                        SolverFGMRES<LA::MPI::BlockVector>::AdditionalData(par->sys.restart)); // With restart of 100

#ifdef LOGRUN
        deallog.push("Outer");
#endif

        constraints.set_zero(distributed_solution);
        solver.solve(system_matrix,
                     distributed_solution,
                     system_rhs,
                     prc);

#ifdef LOGRUN
        deallog.pop();
#endif

        par->sys.itrs.push_back(solver_control.last_step());
    }
    locally_relevant_solution = distributed_solution;

}

void gia::refine_grid(){
    TimerOutput::Scope t(timer, "refine");

    Vector<float> estimated_error_per_cell (triangulation.n_active_cells());

    KellyErrorEstimator<DIM>::estimate (dof_handler,
                                        QGauss<DIM-1>(par->mesh.degree+2),
                                        typename FunctionMap<DIM>::type(),
                                        locally_relevant_solution,
                                        estimated_error_per_cell);

    parallel::distributed::GridRefinement::
            refine_and_coarsen_fixed_number (triangulation,
                                             estimated_error_per_cell,
                                             0.3, 0.03);

    parallel::distributed::SolutionTransfer<DIM, LA::MPI::BlockVector> solTrans(dof_handler);

    // Prepare the refinement process, also prepare the solution transfer.
    triangulation.prepare_coarsening_and_refinement();
    solTrans.prepare_for_coarsening_and_refinement(locally_relevant_solution);

    // Execute the refinement
    triangulation.execute_coarsening_and_refinement ();

    setupDofs();

    // Create a ghost free vector to get the solution interpolation
    LA::MPI::BlockVector tmp(owned_partitioning,
                             mpi_communicator);
    solTrans.interpolate(tmp);
    // Apply constraints and return the solution.
    constraints.distribute(tmp);
    locally_relevant_solution = tmp;
}

void gia::output_results(){
    pcout << PRINT_HEADER("Output results") << endl;
    TimerOutput::Scope t(timer, "Output results");

    // Set solution names
    std::vector<string> solution_names (DIM, "displacements");
    solution_names.push_back ("pressure");

    // Set the type of value DataOut class should expect
    std::vector<DataComponentInterpretation::DataComponentInterpretation>
            data_component_interpretation(DIM,
                                          DataComponentInterpretation::component_is_part_of_vector);
    data_component_interpretation.push_back (DataComponentInterpretation::component_is_scalar);

    DataOut<DIM> data_out;
    data_out.attach_dof_handler (dof_handler);

    // Scale the solution back to the correct values
    locally_relevant_solution *= par->scales.L;

    data_out.add_data_vector (locally_relevant_solution,
                              solution_names,
                              DataOut<DIM>::type_dof_data,
                              data_component_interpretation);

    data_out.build_patches ();

    const string filename = ("solution-" +
                             Utilities::int_to_string
                             (triangulation.locally_owned_subdomain(), 4) + ".vtu");
    std::ofstream output (filename.c_str());
    data_out.write_vtu (output);

    // Write master record for visit and paraview
    if (Utilities::MPI::this_mpi_process(MPI_COMM_WORLD) == 0) {
        std::vector<string> filenames;
        for (unsigned int i=0; i<Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD); ++i)
            filenames.push_back (string("solution-") +
                                 Utilities::int_to_string(i, 4) + ".vtu");

        const string pvtu_master_filename = ("solution.pvtu");
        std::ofstream pvtu_master (pvtu_master_filename.c_str());
        data_out.write_pvtu_record (pvtu_master, filenames);
        const string visit_master_filename = ("solution.visit");
        std::ofstream visit_master (visit_master_filename.c_str());
        data_out.write_visit_record (visit_master, filenames);
    }
}
void gia::output_grid(){
    TimerOutput::Scope t(timer, "Export Grid");
    pcout << PRINT_HEADER("exporting grid") << endl;
    GridOut grid_out;

    ofstream output ("grid.msh");
    grid_out.write_msh(triangulation, output);
}

void gia::run(){
    dealiiTools::ConditionalTable table(Utilities::MPI::this_mpi_process(mpi_communicator) == 0);
    stringstream ss;

    initiate();
    makeGrid();

    // Output the number of used processors
    table.add_value("N_Proc", Utilities::MPI::n_mpi_processes(mpi_communicator));
    int n_cyc = 0;
    // If adaptive, do the cycles
    if(par->mesh.n_adaptive_cycles > 0){
        pcout << "cycle : 0" << endl;
        table.add_value("Cycle", 0);
        n_cyc = par->mesh.n_adaptive_cycles + 1;
    }
    else
        n_cyc = 1;

    setupDofs();

    for(int cycle=0; cycle < n_cyc; cycle++){
        if(cycle > 0){ // If in the first cycle, the mesh should not be refined
            pcout << "-------------------------------------------------" << endl;
            pcout << "cycle : " << cycle << endl;
            table.add_value("Cycle", cycle);
            refine_grid();
        }

        // Terminal output
        table.add_value("Cells", triangulation.n_active_cells());
        table.add_value("Dofs", dof_handler.n_dofs());
        table.add_value("Dofs_A11", dofs_per_block[0]);
        table.add_value("Dofs_Schur", dofs_per_block[1]);

        assemble();
        setup_preconditioner();
        solve();

        table.add_value("It_System", par->sys.mean_itrs());
        table.add_value("It_A11", par->a11.mean_itrs());
        table.add_value("It_Schur", par->a22.mean_itrs());
    }


    if(par->switchs.enable_results){
        output_results ();
        if(Utilities::MPI::n_mpi_processes(mpi_communicator) == 1)
            output_grid();
    }

    if(par->switchs.print_matrices && Utilities::MPI::n_mpi_processes(mpi_communicator) == 1){
        pcout << ALERT(">>> Dumping matrices to file <<<") << endl;
        dealiiTools::File::ex2matlab(system_matrix.block(0,0), "a00");
        dealiiTools::File::ex2matlab(system_matrix.block(0,1), "a01");
        dealiiTools::File::ex2matlab(system_matrix.block(1,0), "a10");
        dealiiTools::File::ex2matlab(system_matrix.block(1,1), "a11");
        dealiiTools::File::ex2matlab(system_precon, "p11");
        dealiiTools::File::ex2matlab(system_rhs.block(0), "rhs0");
        dealiiTools::File::ex2matlab(system_rhs.block(1), "rhs1");
    }

    table.write_text(ss);
    pcout << endl << ss.str() << endl;
}

