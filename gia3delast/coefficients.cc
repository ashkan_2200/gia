#include "coefficients.h"
using dealii::Point;
using std::vector;

using namespace GeoPhysics;

Coefficients::Coefficients(parameters *par) :
    Function<DIM>(),
    _par(par)
{}

double Coefficients::value(const Point<DIM> &/*p*/,
                           const unsigned int /*component*/) const{
    // Change here if layers are needed
    return _par->earth.poisson;
}

void Coefficients::value_list(const vector<Point<DIM> >   &points,
                              vector<double>              &values,
                              const unsigned int          component) const{
    const unsigned int np = points.size();
    for(auto i = 0u; i < np; ++i)
        values[i] = value(points[i], component);
}
