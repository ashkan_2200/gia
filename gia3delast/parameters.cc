
#include "parameters.h"
#include <numeric>
#include "config.h"

using std::ostream;
using std::string;
using std::endl;
using namespace dealii;

void Earth::scale(double L){
    innerRadi /= L;
    outerRadi /= L;
    scale_factor = L;
}

void Earth::declare_prms (ParameterHandler &prm){
    prm.enter_subsection("Earth");{
        prm.declare_entry("outer radius","6.371e6",Patterns::Double(),"Assumed inner radious of the Earth (m)");
        prm.declare_entry("inner radius","3.481e6",Patterns::Double(),"Radious of the Earth (m)");
        prm.declare_entry("density","3300",Patterns::Double(0),"Earth density (Kg/m^3)");
        prm.declare_entry("gravity","-9.8",Patterns::Double(),"Earth gravity (m/s^2)");
        prm.declare_entry("young","4e11",Patterns::Double(0),"Young modulus");
        prm.declare_entry("poisson","0.2",Patterns::Double(0,0.5),"Poisson ratio");
        prm.declare_entry("eta","1e21",Patterns::Double(0),"Viscosity parameter (Pa/s)");
    }
    prm.leave_subsection();
}

void Earth::parse_prms   (ParameterHandler &prm){
    prm.enter_subsection("Earth");{
        outerRadi  = prm.get_double("outer radius");
        innerRadi  = prm.get_double("inner radius");
        density    = prm.get_double("density");
        gravity    = prm.get_double("gravity");
        young      = prm.get_double("young");
        poisson    = prm.get_double("poisson");
        eta        = prm.get_double("eta");
    }
    prm.leave_subsection();
}

void Ice::scale(double L){
    radi /= L;
    height /= L;
    scale_factor = L;
}

void Ice::declare_prms (ParameterHandler &prm){
    prm.enter_subsection("Ice");{
        prm.declare_entry("lua path", "script.lua", Patterns::FileName(), "name of the lua function");
        prm.declare_entry("lua function", "load", Patterns::Anything(), "the load function in the lua file");
        prm.declare_entry("Radius","1.0e6",Patterns::Double(),"Ice radius (m)");
        prm.declare_entry("height","2e3",Patterns::Double(),"Ice height (m)");
        prm.declare_entry("density","917",Patterns::Double(),"Ice density (Kg/m^3)");
    }
    prm.leave_subsection();
}

void Ice::parse_prms   (ParameterHandler &prm){
    prm.enter_subsection("Ice");{
        lua_file = prm.get("lua path");
        lua_func = prm.get("lua function");
        radi = prm.get_double("Radius");
        height = prm.get_double("height");
        density= prm.get_double("density");
    }
    prm.leave_subsection();
}

void Mesh::declare_prms (ParameterHandler &prm){
    prm.enter_subsection("Mesh");{
        prm.declare_entry("degree","1",Patterns::Integer(1),"Degree of polynomial");
        prm.declare_entry("refinement","1",Patterns::Integer(0),"Number of refinements");
        prm.declare_entry("adaptive cycles","0",Patterns::Integer(0),"Number of adaptive cycles");
        prm.declare_entry("initial cells","6",Patterns::Integer(0),"Number of initial cells, could be 6, 12 or 96");
    }
    prm.leave_subsection();
}

void Mesh::parse_prms   (ParameterHandler &prm){
    prm.enter_subsection("Mesh");{
        degree            = prm.get_integer("degree");
        n_refs            = prm.get_integer("refinement");
        n_adaptive_cycles = prm.get_integer("adaptive cycles");
        n_initial_cells   = prm.get_integer("initial cells");
    }
    prm.leave_subsection();

    if(n_initial_cells != 6 && n_initial_cells != 12 && n_initial_cells != 96){
        std::cerr << "The number of initial cells can only be 6, 12 or 96. Choosing 6" << endl;
        n_initial_cells = 6;
    }

}

void AMG::declare_prms (ParameterHandler &prm){
    prm.enter_subsection("amg");{
        prm.declare_entry("aggregation_type","Uncoupled-MIS",Patterns::Anything(),"Aggergation type for the solver");
        prm.declare_entry("aggregation_threshold","0.001",Patterns::Double(0,1),"Aggergation threshold for the solver");
        prm.declare_entry("smoother","Chebyshev");
        prm.declare_entry("coarseMaxSize","2048");
        prm.declare_entry("output_level","0",Patterns::Integer(0,10),"Output level for building amg");
        prm.declare_entry("produce output","false",Patterns::Bool(),"AMG produces construction output");
    }
    prm.leave_subsection();
}

void AMG::parse_prms   (ParameterHandler &prm){
    prm.enter_subsection("amg");{
        aggregation_type = prm.get("aggregation_type");
        threshold        = prm.get_double("aggregation_threshold");
        smoother_type    = prm.get("smoother");
        courseMaxSize    = prm.get_integer("coarseMaxSize");
        verbosLevel      = prm.get_integer("output_level");
        make_output      = prm.get_bool("produce output");
    }
    prm.leave_subsection();
}

int SolveSetting::mean_itrs(){
    int total=0;
    total = std::accumulate(itrs.begin(), itrs.end(), 0);

    return total/itrs.size();
}

void SolveSetting::declare_prms(ParameterHandler &prm){
    std::stringstream ss;
    ss << tol;
    prm.enter_subsection("Solver " + name);{
        prm.declare_entry("tolerance", ss.str(),Patterns::Double(0,1), "tolerance");
        ss.str("");
        ss << n_amg_mult;
        prm.declare_entry("direct amg steps", ss.str(), Patterns::Integer(0), "Number of amg solves before starting the FGMRES");
        ss.str("");
        ss << restart;
        prm.declare_entry("restart", ss.str(), Patterns::Integer(0), "Number of iterations before restart, also number of preallocated vectors");
    }
    prm.leave_subsection();
}

void SolveSetting::parse_prms(ParameterHandler &prm){
    prm.enter_subsection("Solver " + name);{
        tol = prm.get_double("tolerance");
        n_amg_mult = prm.get_integer("direct amg steps");
        restart    = prm.get_integer("restart");
    }
    prm.leave_subsection();
}

void Switch::declare_prms (dealii::ParameterHandler &prm){
    prm.enter_subsection("switchs");{
        prm.declare_entry("output_results","true",Patterns::Bool(),"Output results to file");
        prm.declare_entry("print matrices","false",Patterns::Bool(),"Output matrices");
        prm.declare_entry("weight","false",Patterns::Bool(),"Apply weight of the earth");
        prm.declare_entry("advection","true",Patterns::Bool(),"Apply advection term");
        prm.declare_entry("divergance","false",Patterns::Bool(),"Apply divergance term");
    }
    prm.leave_subsection();
}

void Switch::parse_prms   (dealii::ParameterHandler &prm){
    prm.enter_subsection("switchs");{
        enable_results   = prm.get_bool("output_results");
        print_matrices   = prm.get_bool("print matrices");
        enable_weight    = prm.get_bool("weight");
        enable_advection = prm.get_bool("advection");
        enable_divergance= prm.get_bool("divergance");
    }
    prm.leave_subsection();
}

void parameters::declare_prms(){
    earth.declare_prms(prm);
    ice.declare_prms(prm);
    mesh.declare_prms(prm);
    amg.declare_prms(prm);
    switchs.declare_prms(prm);
    sys.declare_prms(prm);
    a11.declare_prms(prm);
    a22.declare_prms(prm);
}

void parameters::parse_prms(){
    earth.parse_prms(prm);
    ice.parse_prms(prm);
    mesh.parse_prms(prm);
    amg.parse_prms(prm);
    switchs.parse_prms(prm);
    sys.parse_prms(prm);
    a11.parse_prms(prm);
    a22.parse_prms(prm);
}

ostream & parameters::print(ostream &stream){
    prm.print_parameters(stream, ParameterHandler::ShortText);
    return stream;
}

ostream & parameters::prettyPrint(ostream &stream){
    using namespace std;
    ios::fmtflags f(stream.flags());
    int c1 = 15;

    stream << left << endl;
    stream << "************************************" << endl;
    stream << PRINT_HEADER("Parameters:") << endl;

    stream << setw(c1) << "Refinements:" << mesh.n_refs << endl;

    stream << setw(c1) << "Pre-solves :" << sys.n_amg_mult << endl;
    stream << setw(c1) << "Tolerance  :" << endl;

    stream << setw(c1) << "     system:" << sys.tol << endl;
    stream << setw(c1) << "        A11:" << a11.tol << endl;
    stream << setw(c1) << "        A22:" << a22.tol << endl;

    stream << setw(c1) << "Poisson    :" << earth.poisson << endl;
    stream << setw(c1) << "Young      :" << earth.young*scales.S << endl;
    stream << setw(c1) << "Eta        :" << earth.eta << endl;
    stream << setw(c1) << "Earth(Km)  :" << earth.outerRadi*earth.scale_factor*1e-3
                                         << ", " << earth.innerRadi*earth.scale_factor*1e-3 << endl;
    stream << setw(c1) << "Ice(Km)    :" << ice.radi*ice.scale_factor*1e-3
                                         << ", " << ice.height*ice.scale_factor*1e-3 << endl;

    stream << setw(c1) << "Adv/Div    : ";
    stream << (switchs.enable_advection ? "enabled, " : ALERT("disabled, ") );
    stream << (switchs.enable_divergance ? "enabled" : ALERT("disabled") ) << endl;

    stream << setw(c1) << "Weight: ";
    stream << (switchs.enable_weight ? "enabled.": ALERT("disabled.") ) << endl;

    stream << "************************************" << endl;

    stream.flags(f);
    return stream;
}

void parameters::createSampleFile(const string & f_name){
    std::ofstream file_n;
    file_n.open(f_name.c_str());
    prm.print_parameters(file_n, ParameterHandler::Text);
    file_n.close();
}

void parameters::process_file(string _filename){
    if(!_filename.empty())
        prm.read_input(_filename.c_str());

    parse_prms();

    set_data();
}

parameters::parameters()
{
    sys.name = "system";
    sys.tol  = 1e-7;
    sys.n_amg_mult = 5;
    sys.restart = 15;

    a11.name = "a11";
    a11.tol  = 1e-1;
    a11.n_amg_mult = 5;
    a11.restart = 15;

    a22.name = "a22";
    a22.tol  = 1e-1;
    a22.n_amg_mult = 5;
    a22.restart = 15;

    declare_prms();

    parse_prms();

    set_data();
}

void
parameters::set_data(){
    scales.L  = earth.outerRadi;
    scales.U  = 1.0;
    scales.S  = earth.young;
    scales.T  = 1.0;

    // Scale parameters
    earth.abs_gravity  = fabs(earth.gravity);
    earth.young = earth.young/scales.S;
    earth.scale(scales.L);
    ice.scale(scales.L);

    // scaling
    scales.L2_SU = scales.L*scales.L/(scales.S*scales.U);
    scales.L_SU  = scales.L/(scales.S*scales.U);
    scales.RHORGL_S = scales.L_SU*earth.density*earth.gravity;

    weight = (switchs.enable_weight)?(scales.L2_SU*earth.density*earth.gravity):0.0;

}

