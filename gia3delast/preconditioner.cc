#include "preconditioner.h"

#include <fstream>
#include <sstream>
#include <string>
#include <typeinfo>
#include <deal.II/lac/solver_gmres.h>

using namespace dealii;

GeoPhysics::
Preconditioner::Preconditioner (const LA::MPI::BlockSparseMatrix    &A,
                                const LA::MPI::SparseMatrix         &S,
                                const LA::MPI::PreconditionAMG      &Apreconditioner,
                                const LA::MPI::PreconditionAMG      &Spreconditioner,
                                std::vector<IndexSet>               &local_partition,
                                MPI_Comm                            &comunicator,
                                parameters                          *_par)
    :
      a_matrix          (&A),
      s_matrix          (&S),
      a_preconditioner  (Apreconditioner),
      s_preconditioner  (Spreconditioner),
      tmp               (local_partition[1], comunicator),
      par               (_par),
      _DoSolve(false)
{}

void
GeoPhysics::
Preconditioner::set_doSolve(const bool DoSolve){
    _DoSolve = DoSolve;
}

void
GeoPhysics::
Preconditioner::vmult (LA::MPI::BlockVector       &dst,
                       const LA::MPI::BlockVector &src) const
{
    if(_DoSolve){
        // Solver for solving the block with A0^{-1}
        SolverControl control_inv (a_matrix->block(0,0).m(),
                                   par->a11.tol*src.block(0).l2_norm());

#ifdef LOGRUN
        control_inv.enable_history_data ();
        control_inv.log_history (true);
        control_inv.log_result (true);
#endif

        SolverFGMRES<LA::MPI::Vector>
                solver_A (control_inv,
                          SolverFGMRES<LA::MPI::Vector>::AdditionalData(par->a11.restart));

#ifdef LOGRUN
        deallog.push("A1");
#endif
        solver_A.solve(a_matrix->block(0,0),
                       dst.block(0),
                       src.block(0),
                       a_preconditioner);
#ifdef LOGRUN
        deallog.pop();
#endif
        // Push number of inner iterations to solve first block.
        par->a11.itrs.push_back(control_inv.last_step());
    }
    else{
        a_preconditioner.vmult(dst.block(0), src.block(0));
        par->a11.itrs.push_back(1);
    }

    a_matrix->block(1,0).residual(tmp, dst.block(0),src.block(1));

    SolverControl control_s (s_matrix->m(),
                             par->a22.tol*tmp.l2_norm());

#ifdef LOGRUN
    control_s.enable_history_data ();
    control_s.log_history (true);
    control_s.log_result (true);
#endif

    SolverFGMRES<LA::MPI::Vector> // SchurTOL
            solver (control_s,
                    SolverFGMRES<LA::MPI::Vector>::AdditionalData(par->a22.restart));

#ifdef LOGRUN
    deallog.push("Schur");
#endif
    solver.solve(*s_matrix,
                 dst.block(1),
                 tmp,
                 s_preconditioner);
#ifdef LOGRUN
    deallog.pop();
#endif

    // Push number of inner iterations for computing Schure complement.
    par->a22.itrs.push_back(control_s.last_step());
}
