-- Script for ice load
what = "ice model for 3d earth."

-- The global variables L, S, U and grav are passed from the C code,
-- the names should not be changed. If the names are changed the C code should
-- be updated.
L = 1e7
S = 4e11
U = 1

grav = -9.8
ERAD = 6e7


-- These are only inside the script, changing their names are ok
ice_radi = 1e6 -- 1000 Km
ice_dens   = 917
ice_height = 2e3 -- 2 Km

-- This function computes the ice force based on
-- the coordinates x and y
function load(x, y, z)
    dist_to_center = math.sqrt(x*x + (y - ERAD)*(y - ERAD) + z*z)

    if dist_to_center*L < ice_radi then
    	-- y = L/(S*U)*ice_dens*grav*ice_height/L
        force = ice_dens*grav*ice_height/S;
        x = force*x/ERAD
        y = force*y/ERAD
        z = force*z/ERAD
    else
        x = 0
        y = 0
        z = 0
    end
    -- Return values for all the components. This is the saddle point form
    -- which means that we have 4 components in 3d
    return x, y, z, 0
end
