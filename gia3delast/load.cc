#include "load.h"

using namespace GeoPhysics;
using namespace dealii;

Load::Load (parameters *_par) :
    Function<DIM>(DIM+1),
    par(_par)
{
    lf.init_file(par->ice.lua_file);
    lf.init_method(par->ice.lua_func);
    // Send globals that are usefull to lua script
    lf.set_global("L", par->scales.L);
    lf.set_global("S", par->scales.S);
    lf.set_global("U", par->scales.U);
    lf.set_global("grav", par->earth.gravity);
    lf.set_global("ERAD", par->earth.outerRadi);
}

double Load::value (const Point<DIM>   &p, const unsigned int  component) const{
    if(component >= DIM) return 0;

    // Center of the ice.
    Point<DIM> ic(0, par->earth.outerRadi, 0);
    double dist_to_ice_center = 0;
    // Find the distance between the point and the center of the ice
    for(int i = 0; i < DIM;++i)
        dist_to_ice_center += (ic(i) - p(i))*(ic(i) - p(i));

    dist_to_ice_center = sqrt(dist_to_ice_center);

    // If the distance is in the radius of the ice then compute the force for each component
    // Also check that we are on the north pole
    if(dist_to_ice_center < par->ice.radi){
        // http://www.mathalino.com/reviewer/engineering-mechanics/components-of-a-force
        // F_x = F P_x / r
        double force = par->scales.L_SU*par->ice.density*par->earth.gravity*par->ice.height;
        return force*p[component]/par->earth.outerRadi;
    }
    return 0;
}


void Load::vector_value (const Point<DIM> &p, Vector<double>   &values) const{
    lf.vector_value(p, values);
//    for (unsigned int c=0; c < this->n_components; ++c)
//        values(c) = value (p, c);
}
