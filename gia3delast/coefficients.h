#ifndef COEFFICIENTS_H
#define COEFFICIENTS_H

#include <deal.II/base/function.h>

#include <vector>

#include "parameters.h"
#include "config.h"

namespace GeoPhysics {
class Coefficients : public dealii::Function<DIM>
{
public:
    Coefficients(parameters *par);

    double value(const dealii::Point<DIM> &p,
                    const unsigned int component = 0) const override;

    void value_list(const std::vector<dealii::Point<DIM> >   &p,
                    std::vector<double>                      &v,
                    const unsigned int                       component = 0) const override;
private:
    parameters *_par;
};
}

#endif // POISSONCOEFF_H
