#include <deal.II/base/function.h>

#include <fstream>
#include <sstream>
#include <string>
#include <typeinfo>

#ifndef COEFFICIENT_H
#define COEFFICIENT_H

#include "parameters.h"
#include "config.h"

namespace GeoPhysics
{
    class Coefficients : public dealii::Function<DIM>
    {
    public:
        Coefficients (parameters *_par);


        virtual double value (const dealii::Point<DIM>   &p, const unsigned int  component = 0) const override;

        virtual void value_list (const std::vector<dealii::Point<DIM> > &points,
                                    std::vector<double>            &values,
                                    const unsigned int              component = 0) const override;

    private:
        // pointer to parameter object
        parameters *par;
    };
}

#endif
