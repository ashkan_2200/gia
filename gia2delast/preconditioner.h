/*
 * Author:  Ali Dorostkar
 */

#ifndef PRECONDITIONER_H
#define PRECONDITIONER_H

#include "parameters.h"
#include "config.h"

namespace GeoPhysics
{

// new code step-31
class Preconditioner : public dealii::Subscriptor
{
public:
    Preconditioner (const LA::MPI::BlockSparseMatrix   &A,
                    const LA::MPI::SparseMatrix        &S,
                    const LA::MPI::PreconditionAMG     &Apreconditioner,
                    const LA::MPI::PreconditionAMG     &Spreconditioner,
                    std::vector<dealii::IndexSet>      &local_partition,
                    MPI_Comm                           &comunicator,
                    parameters                         *_par);

    void vmult (LA::MPI::BlockVector       &dst,
                const LA::MPI::BlockVector &src) const;
private:
    // pointer to parameter object
    const dealii::SmartPointer<const LA::MPI::BlockSparseMatrix> a_matrix;
    const dealii::SmartPointer<const LA::MPI::SparseMatrix>      s_matrix;
    const LA::MPI::PreconditionAMG  &a_preconditioner;
    const LA::MPI::PreconditionAMG  &s_preconditioner;
    mutable LA::MPI::Vector tmp;

    parameters *par;
};
}

#endif // PRECONDITIONER_H
