
#ifndef GIA_H
#define GIA_H

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/base/timer.h>
#include <deal.II/base/index_set.h>
#include <deal.II/distributed/tria.h>
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/lac/full_matrix.h>

#include "config.h"
#include "parameters.h"

namespace GeoPhysics {

class gia{
public:
    gia(parameters *_par);
    ~gia();

    void run();
private:
    parameters *par;

    MPI_Comm                                  mpi_communicator;
    dealii::ConditionalOStream                        pcout;
    dealii::TimerOutput                               timer;

    dealii::parallel::distributed::Triangulation<DIM> triangulation;
    dealii::DoFHandler<DIM>                           dof_handler;
    dealii::FESystem<DIM>                             fe;
    dealii::ConstraintMatrix                        constraints;

    dealii::IndexSet                                  locally_owned_dofs;
    dealii::IndexSet                                  locally_relevant_dofs;
    std::vector<dealii::IndexSet>                     owned_partitioning;
    std::vector<dealii::IndexSet>                     relevant_partitioning;

    LA::MPI::BlockSparseMatrix                system_matrix;
    LA::MPI::SparseMatrix                     system_precon;
    LA::MPI::BlockVector                      locally_relevant_solution;
    LA::MPI::BlockVector                      system_rhs;
    dealii::std_cxx1x::shared_ptr<LA::MPI::PreconditionAMG> A_preconditioner;
    dealii::std_cxx1x::shared_ptr<LA::MPI::PreconditionAMG> S_preconditioner;
    std::vector<dealii::types::global_dof_index> dofs_per_block;

    // Methods
    void initiate();
    void makeGrid();
    void setupDofs();
    void assemble();
    void setup_preconditioner();
    void solve();
    void refine_grid();
    void output_results();
};

}

#endif
