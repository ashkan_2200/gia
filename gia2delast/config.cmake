// This file contains preprocessor definitions and other configurations and 
// Is generated during the cmake phase.
// Author: Ali Dorostkar

// Log the iteration count and other dealii related values if defined
#cmakedefine LOGRUN

#cmakedefine USE_PETSC
#cmakedefine USE_TRILINOS

#define DIM 2
// Other settings

// set to use trilinos of petsc
#include <deal.II/lac/generic_linear_algebra.h>
namespace LA {
#if defined(DEAL_II_WITH_TRILINOS) && defined(USE_TRILINOS)
using namespace dealii::LinearAlgebraTrilinos;
#elif defined(DEAL_II_WITH_PETSC) && defined(USE_PETSC)
using namespace dealii::LinearAlgebraPETSc;
#else
#error DEAL_II_WITH_PETSC or DEAL_II_WITH_TRILINOS required
#endif
}

/*!
 * Define colors depending on the operating system the program is compiled on.
 */
#if defined(__APPLE__) || defined(__linux__)
#define RESET   "\033[0m"
#define RED     "\033[31m"
#define GREEN   "\033[32m"
#define YELLOW  "\033[33m"
#define BOLDRED     "\033[1m\033[31m"
#define BOLDGREEN   "\033[1m\033[32m"
#define BOLDYELLOW  "\033[1m\033[33m"
#else
#define RESET   ""
#define RED     ""
#define GREEN   ""
#define BOLDRED     ""
#define BOLDGREEN   ""
#endif

// Color coded alerts
#define ALERT(x) RED x RESET
#define WARN(x) YELLOW x RESET
#define TELL(x) GREEN x RESET

//#define PRINT_HEADER(x) RED x RESET
#define PRINT_HEADER(x) "---> " x
