-- Script for ice load
what = "ice model for 2d earth."

-- The global variables L, S, U and grav are passed from the C code, 
-- the names should not be changed. If the names are changed the C code should 
-- be updated.
L = 1e7
S = 4e11
U = 1

grav = -9.8

ice_length = 1e6 -- 1000 Km
ice_dens   = 917
ice_height = 2e3 -- 2 Km

-- This function computes the ice force based on 
-- the coordinates x and y
function load(x, y)
    if x*L <= ice_length then
        -- y = L/(S*U)*ice_dens*grav*ice_height/L
        y = ice_dens*grav*ice_height/S
    else
        y = 0
    end
    -- return values for all the components. This is the saddle point form
    -- which means that we have 3 components in 2d
    return 0, y, 0
end
