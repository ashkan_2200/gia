/*
 * Author:  Ali Dorostkar
 * Date:    2014-09-18
 * Feature: Parameter handler class.
 */
/*
 *  Date   |  Change
 * --------+------------------------------------------------------------
 * 140918  | - Initial release using the deal parameter handler
 * 140918  | - Added all parameters to declare_parameters()
 * 141002  | - Added PRINT_HEADER method to unify the output format.
 * 141002  | - extracted from the different gia files to have a
 *         |      unique parameter class everywhere.
 * 141002  | - Added a get_iterations method
 * 160819  | - Refactoring, parameters are now declared in their related structure
 * 160819  | - Changed from singlton to normal class, adds the oportunity to test
 *         |      multiple settings in one run
 * ---------------------------------------------------------------------
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

#include <deal.II/base/parameter_handler.h>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <string.h>
#include <sstream>
#include <sys/stat.h>

//using namespace dealii;

/*! Boundary types enumerator,
 * Different available boundaries.
 * Used power of two to be able to
 * use multiple flags.
 */
struct bFlags{
    enum boundary_Type {
        NEUMANN = 1<<0,
        NO_SLIP = 1<<1,
        V_SLIP  = 1<<2,
        LOAD    = 1<<3,
        FREE    = 1<<4
    };
};

/*!
 * \brief The Earth struct : Gathers all earth related data
 */
struct Earth{
    double density;
    double gravity, abs_gravity;
    double length, depth;
    double scale_factor;
    double young, poisson, eta;

    void scale(double L);
    void declare_prms (dealii::ParameterHandler &prm);
    void parse_prms   (dealii::ParameterHandler &prm);
};

/*!
 * \brief The Ice struct : Gathers all ice related data
 */
struct Ice{
    double density;
    double length, height;
    double scale_factor;
    void scale(double L);
    std::string lua_file;
    std::string lua_func;
    void declare_prms (dealii::ParameterHandler &prm);
    void parse_prms   (dealii::ParameterHandler &prm);
};

struct Mesh{
    int degree, n_refs, n_adaptive_refinements;
    // length, depth divisions
    int initial_length_divisions;
    int initial_depth_divisions;
    bFlags::boundary_Type b_ice, b_up, b_left, b_right, b_bottom;

    bFlags::boundary_Type str2boundary(std::string tempSt);
    void declare_prms (dealii::ParameterHandler &prm);
    void parse_prms   (dealii::ParameterHandler &prm);
};

struct AMG{
    std::string aggregation_type, smoother_type;
    double threshold;
    int courseMaxSize, verbosLevel;
    bool make_output;

    void declare_prms (dealii::ParameterHandler &prm);
    void parse_prms   (dealii::ParameterHandler &prm);
};

struct SolveSetting{
    std::string name="not set";
    double tol=1e-7;
    int n_amg_mult=0;
    int restart=30;

    std::vector<unsigned int> itrs;
    int mean_itrs();

    void declare_prms (dealii::ParameterHandler &prm);
    void parse_prms   (dealii::ParameterHandler &prm);
};

struct Switch{
    bool enable_results, enable_weight,
         enable_advection, enable_divergance, print_matrices;

    void declare_prms (dealii::ParameterHandler &prm);
    void parse_prms   (dealii::ParameterHandler &prm);
};

struct SCALES{
    double L, U, S, T;
    /*!
     * \brief scale1 = L^2/(SU)
     * \brief scale2 = L/SU
     * \brief scale3 = (L/S)*rho_r*g
     */
    double L2_SU, L_SU, RHORGL_S;
};

class parameters {
public:
    Earth earth;
    Ice ice;
    Mesh mesh;
    AMG amg;
    Switch switchs;
    SCALES scales;
    double weight;

    SolveSetting sys, a11, a22;

    parameters();
    std::string get_iterations();
    std::ostream & print(std::ostream &);
    std::ostream & prettyPrint(std::ostream &);
    void createSampleFile(const std::string &);
    void process_file(std::string _filename);
private:
    dealii::ParameterHandler prm;
    // Methods

    void set_data();
    void declare_prms();
    void parse_prms  ();
};

#endif /* PARAMETERS_H_ */
