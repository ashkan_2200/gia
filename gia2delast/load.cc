#include "load.h"

using namespace GeoPhysics;
using namespace dealii;

Load::Load (parameters *_par) :
    Function<DIM>(DIM+1),
    par(_par)
{
    lf.init_file(par->ice.lua_file);
    lf.init_method(par->ice.lua_func);
    // Send globals that are usefull to lua script
    lf.set_global("L", par->scales.L);
    lf.set_global("S", par->scales.S);
    lf.set_global("U", par->scales.U);
    lf.set_global("grav", par->earth.gravity);
}

double Load::value (const Point<DIM>   &p, const unsigned int  component) const{
    Assert (component < this->n_components, ExcIndexRange (component, 0, this->n_components));

    if (p[0] <= par->ice.length && component == 1)
        return par->scales.L_SU*par->ice.density*par->earth.gravity*par->ice.height;
    return 0;
}


void Load::vector_value (const Point<DIM> &p, Vector<double> &values) const{
    lf.vector_value(p, values);
//    values(DIM) = 0;
//    for (unsigned int c=0; c < this->n_components; ++c)
//        values(c) = value (p, c);
}
