#include "coefficient.h"

using namespace GeoPhysics;
using namespace dealii;

Coefficients::Coefficients (parameters *_par)  :
    Function<DIM>(),
    par(_par)
{}

double Coefficients::value (const Point<DIM> &/*p*/,
                            const unsigned int /*component*/) const
{
    return par->earth.poisson;
}

void Coefficients::value_list (const std::vector<Point<DIM> > &points,
                                  std::vector<double>            &values,
                                  const unsigned int              component) const
{
    Assert (values.size() == points.size(), ExcDimensionMismatch (values.size(), points.size()));
    const unsigned int n_points = points.size();

    for (unsigned int i=0; i<n_points; ++i)
    {
        values[i] = value(points[i], component);
    }
}
