/*
 * Author:  Ali Dorostkar
 * Date:    2014-09-01
 * Feature: Main program that calls gia classes. V2.0
 */
/*
 *  Date   |  Change
 * --------+------------------------------------------------------------
 * 140901  | Inital code written
 * 140904  | Problem specification also flushed to deallog
 * 160819  | Rewrite of the new code started
 * ---------------------------------------------------------------------
 */
// TODO:

#include "gia.h"
#include "deal.II/base/utilities.h"
#include "deal.II/base/logstream.h"
#include "parameters.h"

int main (int argc, char** argv)
{
    using namespace dealii;
    using namespace std;

    parameters par;
    int width = 30;

    try{
        // Initialize MPI
#if defined(USE_PETSC)
        Utilities::MPI::MPI_InitFinalize mpi_initialization (argc, argv, 1);
#elif defined(USE_TRILINOS)
        Utilities::MPI::MPI_InitFinalize mpi_initialization (argc, argv, 1);
#endif

        int opt;
        while ((opt = getopt (argc, argv, "hcf:")) != -1){
            switch (opt) {
            case 'f':
                par.process_file(optarg);
                break;
            case 'c':
                par.createSampleFile("default.cfg");
                return EXIT_SUCCESS;
            case '?':
            default:
                cerr << "Unknown options" << endl;
                cerr << "Usage: gia [-f filename] [-c]" << endl;
                cerr << "           -f: parameter file" << endl;
                cerr << "           -c: create default file" << endl;
                return EXIT_SUCCESS;
            }
        }

        string filename = "gia.log";
        ofstream logFile(filename.c_str());

        // Attach deallog to process output
        deallog.attach(logFile);
        deallog.depth_console (0);

        stringstream ss;
        par.prettyPrint(ss);
        deallog << ss.str() << flush;

        GeoPhysics::gia gia_problem(&par);
        gia_problem.run ();
    }
    catch (exception &exc){
        cerr << setw(width) << setfill('-') << "" << endl;
        cerr << "Exception on processing: " << endl
             << exc.what() << endl
             << "Aborting!" << endl
             << setw(width) << setfill('-') << "" << endl;

        return EXIT_FAILURE;
    }
    catch (...){
        cerr << setw(width) << setfill('-') << "" << endl;
        cerr << "Unknown exception!" << endl
             << "Aborting!" << endl
             << setw(width) << setfill('-') << "" << endl;
        return EXIT_FAILURE;
    }
    return 0;
}
